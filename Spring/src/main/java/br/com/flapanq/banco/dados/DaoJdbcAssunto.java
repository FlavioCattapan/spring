package br.com.flapanq.banco.dados;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class DaoJdbcAssunto extends JdbcDaoSupport {

	public int somaTotalAssuntos() {
		return getJdbcTemplate().queryForInt("SELECT count(*) FROM  assunto");
	}

	public Long getIdAssunto(String nome, String descricao) {
		return getJdbcTemplate().queryForLong(
				"SELECT id FROM  assunto  where nome = ? and descricao = ? ",
				nome, descricao);
	}

	public void inserir(Assunto assunto) {
		Map<String, Object> paramentros = new HashMap<String, Object>();
		paramentros.put("nome", assunto.getNome());
		Long id = new SimpleJdbcInsert(getDataSource())
				.withTableName("assunto").usingGeneratedKeyColumns("id")
				.executeAndReturnKey(paramentros).longValue();
		assunto.setId(id);

	}

	public void update(Assunto assunto) {
		getJdbcTemplate().update(
				"update assunto set nome = ?, descricao = ? where id = ?",
				assunto.getNome(), assunto.getDescricao(), assunto.getId());
	}

	public List<Assunto> list(String nome, String descricao) {

		String[] parametros = { nome, descricao };

		return getJdbcTemplate()
				.query("SELECT * FROM  assunto  where nome like ? and descricao like ? ",
						parametros, new RowMapper<Assunto>() {

							public Assunto mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								Assunto assunto = new Assunto();
								assunto.setId(rs.getLong(3));
								assunto.setNome(rs.getString(2));
								assunto.setDescricao(rs.getString(3));
								return assunto;
							}

						}

				);

	}
	
	public void importarUsuarios(){
		
		final List<Assunto> listaAssunto = criarAssuntos();
		
		BatchPreparedStatementSetter batchPreparedStatementSetter = new BatchPreparedStatementSetter() {
			
			public void setValues(PreparedStatement ps, int i) throws SQLException {

				Assunto assunto = listaAssunto.get(i);
				ps.setString(1, assunto.getNome());
				ps.setString(2, assunto.getDescricao());

			}
			
			public int getBatchSize() {
				return listaAssunto.size();
			}
		};
		
		getJdbcTemplate().batchUpdate("insert into assunto (nome, descricao) values(?,?)", batchPreparedStatementSetter);
		
	}
	
    private List<Assunto> criarAssuntos(){
    	
    	List<Assunto>  listaAssunto = new ArrayList<Assunto>();
    	
    	for(int i = 0; i < 50 ; i++){
    		
    		Assunto assunto = new Assunto();
            assunto.setNome("Nome"+i);
            assunto.setDescricao("Descricao"+i);
            listaAssunto.add(assunto);
    	}
    	
    	return listaAssunto;
    	
    	
    	
    	
    	
    	
    }
}
