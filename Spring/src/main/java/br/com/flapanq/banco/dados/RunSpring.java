package br.com.flapanq.banco.dados;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;

public class RunSpring {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"bancodados.xml");

		DaoJdbcAssunto daoJdbcAssunto = context.getBean("daoJdbcAssunto", DaoJdbcAssunto.class);
		System.out.println(daoJdbcAssunto.somaTotalAssuntos());
		
		try {
			System.out.println(daoJdbcAssunto.getIdAssunto("Fl�vio", "particular"));
		} catch (EmptyResultDataAccessException e) {
				System.out.println("Dados n�o encontrados");
		}
		System.out.println(daoJdbcAssunto.list("%", "%"));
		Assunto assunto = new Assunto();
		assunto.setId(1L);
		assunto.setDescricao("new");
		assunto.setNome("Fred");
		daoJdbcAssunto.update(assunto);
		
		Assunto assunto2 = new Assunto();
		assunto2.setNome("Kroig");
		daoJdbcAssunto.inserir(assunto2);
		System.out.println(assunto2.getId());
		
		daoJdbcAssunto.importarUsuarios();
		
	}

}
