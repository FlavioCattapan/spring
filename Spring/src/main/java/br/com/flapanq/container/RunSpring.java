package br.com.flapanq.container;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import br.com.flapanq.container.HelloWorld;

public class RunSpring {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"spring-container.xml");

		HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		obj.printHello();

		FonteDadosArquivo fonteDadosArquivo = (FonteDadosArquivo) context
				.getBean("fonteDadosArquivo");
		System.out.println(fonteDadosArquivo.getArquivo());

		// passando a classe como agumento n�o precisa fazer o cast
		Processador processador = context.getBean("processador",
				Processador.class);
		System.out.println(processador.getFonteDadosArquivo().getArquivo());

		// obtendo o bean pelo tipo da classe
		Processador processador2 = context.getBean(Processador.class);
		processador2.getFonteDadosArquivo().getArquivo();

		ProcessadorInjectConstructor processadorInjectConstructor = (ProcessadorInjectConstructor) context
				.getBean("processadorConstructorOrdem");
		System.out.println(processadorInjectConstructor.getFonteDadosArquivo()
				.getArquivo());

		ProcessadorInjectConstructor processadorInjectConstructorForaOrdem = (ProcessadorInjectConstructor) context
				.getBean("processadorConstructorForaOrdem");
		System.out.println(processadorInjectConstructorForaOrdem
				.getFonteDadosArquivo().getArquivo());

		ImpressorImpl impressorImpl = (ImpressorImpl) context
				.getBean("impressorName2");
		System.out.println(impressorImpl.getName());

		FonteDadosArquivo fonteDadosArquivoFactory = (FonteDadosArquivo) context
				.getBean("fonteDadosInstancia");
		System.out.println(fonteDadosArquivoFactory.getArquivo());

		// inicializando as listas da classe
		FonteDados inicializandoListas = (FonteDados) context
				.getBean("inicializandoListasMaps");
		System.out.println(inicializandoListas.getArquivos());
		System.out.println(inicializandoListas.getArquivosFile().get(0)
				.getPath());
		System.out.println(inicializandoListas.getMapa().get("string"));
		System.out.println(((ImpressorImpl) inicializandoListas.getMapa().get(
				"impressor")).getName());

		// obtendo os beans pelo tipo
		Map<String, FonteDados> mapFonteDados = context
				.getBeansOfType(FonteDados.class);
		System.out.println(mapFonteDados.entrySet());

		// comprovando o scopo singleton (default)
		HelloWorld obj2 = (HelloWorld) context.getBean("helloBean");
		HelloWorld obj3 = (HelloWorld) context.getBean("helloBean");
		System.out.println("Escopo singleton = " + (obj2 == obj3));

		// comprovando o scopo prototype 
		HelloWorld obj4 = (HelloWorld) context.getBean("helloBeanPrototype");
		HelloWorld obj5 = (HelloWorld) context.getBean("helloBeanPrototype");
		System.out.println("Escopo prototype = " + (obj4 == obj5));
		
		// inicializa��o tardia pode ser configurado no bean ou para todos os beans default-init-method="true" 
		HelloWorld obj6 = (HelloWorld) context.getBean("helloBeanTardia");
		obj6.printHello();
		
		// metodo sendo inicializado quando o bean for constru�do definindo em todos os beans default-init-method="init"
		HelloWorld obj7 = (HelloWorld) context.getBean("helloBeanInitMethod");
		obj7.printHello();
		
		// declarado no arquivo de import
		HelloWorld obj8 = (HelloWorld) context.getBean("helloBeanImport");
		obj8.printHello();
		
		//heran�a
		HelloWorld obj9 = (HelloWorld) context.getBean("helloBeanImportInheritance");
		obj9.printHello();
		
		// Expression Language
		ExpressionLanguage expressionLanguage = context.getBean("expressionLanguage", ExpressionLanguage.class);
		System.out.println(expressionLanguage.getMaxNumArquivos());
		System.out.println(expressionLanguage.getBoolean1());
		System.out.println(expressionLanguage.getName());
		System.out.println(expressionLanguage.getProp());
		


	}

}
