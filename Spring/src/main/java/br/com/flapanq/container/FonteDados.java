package br.com.flapanq.container;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface FonteDados {
	
	List<String> getArquivos();
	
	void setArquivos(List<String> arquivos);
	
	List<File> getArquivosFile();
	
	Map<String, Object> getMapa();
	
	
	

}
