package br.com.flapanq.container;

import java.io.File;
import java.util.List;
import java.util.Map;

public class FonteDadosArquivo implements FonteDados {

	private String arquivo;
	
	private List<String> arquivos;
	
	private List<File> arquivosFile;
	
	private Map<String, Object> mapa;

	public Map<String, Object> getMapa() {
		return mapa;
	}

	public void setMapa(Map<String, Object> mapa) {
		this.mapa = mapa;
	}

	public List<File> getArquivosFile() {
		return arquivosFile;
	}

	public void setArquivosFile(List<File> arquivosFile) {
		this.arquivosFile = arquivosFile;
	}

	public List<String> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<String> arquivos) {
		this.arquivos = arquivos;
	}

	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	
	

}
