package br.com.flapanq.container;

public class HelloWorld {
	
	private String name;
	
	public void init(){
		name = "init method is calling";
	}
	
	public void cleanup(){
		System.out.println("cleanup");
	}

	public void setName(String name) {
		this.name = name;
	}

	public void printHello() {
		System.out.println("Hello ! " + name);
	}

	public String getName() {
		return name;
	}
	
	 

}
