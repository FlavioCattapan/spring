package br.com.flapanq.container;

public class ProcessadorInjectConstructor {
	
	private ImpressorImpl impressor;
	
	private FonteDadosArquivo fonteDadosArquivo;
	

	public ProcessadorInjectConstructor(ImpressorImpl impressor,
			FonteDadosArquivo fonteDadosArquivo) {
		super();
		this.impressor = impressor;
		this.fonteDadosArquivo = fonteDadosArquivo;
	}

	public ImpressorImpl getImpressor() {
		return impressor;
	}

	public void setImpressor(ImpressorImpl impressor) {
		this.impressor = impressor;
	}

	public FonteDadosArquivo getFonteDadosArquivo() {
		return fonteDadosArquivo;
	}

	public void setFonteDadosArquivo(FonteDadosArquivo fonteDadosArquivo) {
		this.fonteDadosArquivo = fonteDadosArquivo;
	}


	
	
	

}
