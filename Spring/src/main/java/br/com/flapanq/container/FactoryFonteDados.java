package br.com.flapanq.container;

public class FactoryFonteDados {
	
	public FonteDadosArquivo createFonteDados(){
		FonteDadosArquivo fonteDadosArquivo = new FonteDadosArquivo();
		fonteDadosArquivo.setArquivo("Arquivo factory");
		return fonteDadosArquivo;
	}

}
