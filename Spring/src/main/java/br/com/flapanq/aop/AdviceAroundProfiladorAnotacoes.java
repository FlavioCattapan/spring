package br.com.flapanq.aop;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class AdviceAroundProfiladorAnotacoes {

	private List<Long> tempos = new ArrayList<Long>();
	
	@Around("execution(* br.com.flapanq.aop.DaoCliente.listar(..))")
	private Object profilar(ProceedingJoinPoint proceedingJoinPoint)throws Throwable{
		
		long momentoInicial = System.currentTimeMillis();
		Object resultado = proceedingJoinPoint.proceed();
		long tempo = System.currentTimeMillis() - momentoInicial;
		tempos.add(tempo);
		System.out.println("Tempo para executar" + tempo + "ms");
		return resultado;
		
	}
	

}
