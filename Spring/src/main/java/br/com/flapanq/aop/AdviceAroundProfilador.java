package br.com.flapanq.aop;

import java.util.ArrayList;
import java.util.List;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class AdviceAroundProfilador implements MethodInterceptor {

	private List<Long> tempos = new ArrayList<Long>();
	
	public List<Long> getTempos() {
		return tempos;
	}

	public Object invoke(MethodInvocation invocation) throws Throwable {
		System.out.println("Invoque in�cio");
		long momentoInical = System.currentTimeMillis();
		// objeto retornado do metodo
		Object resultado = invocation.proceed();
		long tempo = System.currentTimeMillis() - momentoInical;
		getTempos().add(tempo);
		System.out.println("Tempo para executar " + tempo + "ms" );
		System.out.println("Invoque fim");
		return resultado;
	}

	
	

}
