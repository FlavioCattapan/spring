package br.com.flapanq.aop;

import org.springframework.aop.ThrowsAdvice;

public class AdviceAfterThrows implements ThrowsAdvice {


	public void afterThrowing(IllegalArgumentException e) throws Throwable {
		System.out.println("HijackThrowException : Throw exception hijacked!");
	}
	
	
	
	
	
}
