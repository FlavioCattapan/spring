package br.com.flapanq.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RunSpring {

	public static void main(String[] args) {


		ApplicationContext context = new ClassPathXmlApplicationContext(
				"aop.xml");
		
		try {
			DaoPessoa daoPessoa = context.getBean("daoPessoa", DaoPessoa.class);
			daoPessoa.persistir(1);
			daoPessoa.persistir(2);
			
			DaoPessoa daoPessoa2 = context.getBean("daoPessoa", DaoPessoa.class);
			daoPessoa2.persistir(3);
			daoPessoa2.printThrowException();
		} catch (Exception e) {
		}
		
		AdviceAroundProfilador adviceAroundProfilador = context.getBean(AdviceAroundProfilador.class);
		
		System.out.println(adviceAroundProfilador.getTempos());
		
		DaoCliente daoCliente = context.getBean("daoCliente", DaoCliente.class);
		
		daoCliente.listar();
		
		
		

	}

}
