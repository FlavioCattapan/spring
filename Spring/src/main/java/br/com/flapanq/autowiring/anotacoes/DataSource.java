package br.com.flapanq.autowiring.anotacoes;

import org.springframework.stereotype.Service;

@Service
public class DataSource {

	private String connectionString = "Default";

	public String getConnectionString() {
		return connectionString;
	}

	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}
	
	
	
	
}
