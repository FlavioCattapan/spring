package br.com.flapanq.autowiring.anotacoes;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RunSpring {
	
	public static void main(String[] args) {
		
		// autowiring
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"autowiring.xml");
		
		DaoProduto produto = context.getBean("daoProduto", DaoProduto.class);
		System.out.println(produto.getDataSource().getConnectionString());
		
		DaoProduto produtoOracle = context.getBean("daoProdutoOracle", DaoProduto.class);
		System.out.println(produtoOracle.getDataSource().getConnectionString());
		
		ApplicationContext contextAnnotations = new ClassPathXmlApplicationContext(
				"anotacoes.xml");
		
		// Annotations
		AnnotationsDao annotationsDao = contextAnnotations.getBean("annotationsDao", AnnotationsDao.class);
		System.out.println(annotationsDao.getDataSource().getConnectionString());
		System.out.println(annotationsDao.getDataSource2().getConnectionString());
		System.out.println(annotationsDao.getDataSource3().getConnectionString());
		System.out.println(annotationsDao.getNammedAnnot().getNammedAnnot());
		System.out.println("Prototype :" + (annotationsDao.getNammedAnnot() != annotationsDao.getNammedAnnot2()));
		
		
		contextAnnotations = null;
	}

	
}
