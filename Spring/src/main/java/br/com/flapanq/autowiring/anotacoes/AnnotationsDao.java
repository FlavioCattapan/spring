package br.com.flapanq.autowiring.anotacoes;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

@Service
public class AnnotationsDao {
	
	private DataSource dataSource;
	
	@Inject
	private DataSource dataSource2;
	
	@Resource
	private DataSource dataSource3;
	
	@Named("nammedAnnot")
	private NammedAnnot nammedAnnot;
	
	@Named("nammedAnnot")
	private NammedAnnot nammedAnnot2;
	
	public NammedAnnot getNammedAnnot2() {
		return nammedAnnot2;
	}

	public void setNammedAnnot2(NammedAnnot nammedAnnot2) {
		this.nammedAnnot2 = nammedAnnot2;
	}

	public NammedAnnot getNammedAnnot() {
		return nammedAnnot;
	}

	public void setNammedAnnot(NammedAnnot nammedAnnot) {
		this.nammedAnnot = nammedAnnot;
	}

	public DataSource getDataSource3() {
		return dataSource3;
	}

	public void setDataSource3(DataSource dataSource3) {
		this.dataSource3 = dataSource3;
	}

	public DataSource getDataSource2() {
		return dataSource2;
	}

	public void setDataSource2(DataSource dataSource2) {
		this.dataSource2 = dataSource2;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	@Required @Autowired
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	

}
