package br.com.flapanq.autowiring.anotacoes;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

@Scope("prototype")
@Named("nammedAnnot")
public class NammedAnnot {


	@PostConstruct
	private void init(){
		System.out.println("Init");
	}
	
	@PreDestroy
	private void destroy(){
		System.out.println("Destroy");
	}
	
	private String nammedAnnot = "nammedAnnot";

	public String getNammedAnnot() {
		return nammedAnnot;
	}

	public void setNammedAnnot(String nammedAnnot) {
		this.nammedAnnot = nammedAnnot;
	}
	
	
	
}
